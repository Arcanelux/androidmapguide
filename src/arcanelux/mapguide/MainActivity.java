package arcanelux.mapguide;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements OnClickListener {
	private String TAG = this.getClass().getName();
	private GoogleMap mMap;
	private EditText etAddress;
	private Button btnFind;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		etAddress = (EditText) findViewById(R.id.etAddress);
		btnFind = (Button) findViewById(R.id.btnFind);
		btnFind.setOnClickListener(this);

		etAddress.setText("테헤란로 311");

		setUpMapIfNeeded();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
					.getMap();
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnFind:
			Log.d(TAG, "Find");
			String address = etAddress.getText().toString();
			new GeoPointTask(address).execute();
			break;
		}
	}


	// 주소 검색하고 마커꼽고 카메라 이동하는 AsyncTask
	private class GeoPointTask extends AsyncTask<Void, Void, Void> {
		private String address;
		
		public GeoPointTask(String address){
			this.address = address;
		}
		
		LatLng blat;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			blat = getGeoPoint(getLocationInfo(address.replace("\n", " ").replace(" ", "%20")));  //주소를 넘겨준다(공백이나 엔터는 제거합니다)
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.d("myLog", "경도:" + blat.longitude); //위도/경도 결과 출력
			Log.d("myLog", "위도:" + blat.latitude );
			
			// 마커추가
			mMap.addMarker(new MarkerOptions().position(new LatLng(blat.latitude, blat.longitude)).title(address));
			
			// 카메라 이동
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(blat, 10));
		}
	}

	public static JSONObject getLocationInfo(String address) {
		HttpGet httpGet = new HttpGet(
				"http://maps.google.com/maps/api/geocode/json?address="
						+ address + "&ka&sensor=false"); 
		//해당 url을 인터넷창에 쳐보면 다양한 위도 경도 정보를 얻을수있다(크롬 으로실행하세요)
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonObject;
	}


	public static LatLng getGeoPoint(JSONObject jsonObject) {
		LatLng location = null;
		Double lon = new Double(0);
		Double lat = new Double(0);

		try {
			lon = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getJSONObject("geometry").getJSONObject("location")
					.getDouble("lng");

			lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
					.getJSONObject("geometry").getJSONObject("location")
					.getDouble("lat");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		location = new LatLng(lat, lon);
		return location;
	}
}
